package mqttutil

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"io/ioutil"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/prometheus/common/log"
)

func ConnectToBroker(brokerConfig Broker) (mqtt.Client, error) {

	opts := createOpts(brokerConfig)
	client := mqtt.NewClient(opts)

	if token := client.Connect(); token.Wait() && token.Error() != nil {
		return nil, token.Error()
	}

	return client, nil
}

func SubscribeToSingleTopic(cli mqtt.Client, topic string, qos byte, handler mqtt.MessageHandler) error {
	if token := cli.Subscribe(topic, qos, handler); token.Wait() && token.Error() != nil {
		return token.Error()
	}

	return nil
}

func JoinTopic(topics ...string) string {
	var fullTopic bytes.Buffer

	for i, topic := range topics {
		// Length check
		if len(topic) == 0 {
			continue
		}

		// Strip ending slash if it exists
		if topic[len(topic)-1] == '/' {
			topic = topic[0 : len(topic)-1]
		}

		// Make sure the topic was not just a single "/"
		if len(topic) == 0 {
			continue
		}

		// Don't check for leading "/" on first topic
		if i != 0 {
			// Check and remove leading "/"
			if topic[0] == '/' {
				topic = topic[1:]
			}

			// Skip if the topic is empty
			if len(topic) == 0 {
				continue
			}

			fullTopic.WriteString("/")
		}

		fullTopic.WriteString(topic)
	}

	return fullTopic.String()
}

func createOpts(config Broker) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()

	if config.ClientID != "" {
		opts.SetClientID(config.ClientID)
	}

	// If the cert and key exists, we can create connect with cert auth
	tls, err := loadClientCertFile(config.KeyFile, config.CertFile, config.CaChainFile)

	if err != nil {
		log.Errorf("Error loading certificate data: %s", err)
	} else {
		opts.SetTLSConfig(tls)
	}

	if config.Username != "" {
		opts.SetUsername(config.Username)
	}

	if config.Password != "" {
		opts.SetPassword(config.Password)
	}

	opts.SetCleanSession(config.PersistentSession)

	opts.AddBroker(config.BrokerURI)

	return opts
}

func loadClientCertFile(key, cert, caCerts string) (*tls.Config, error) {
	if key == "" || cert == "" {
		return nil, errors.New("No key/cert pair specified")
	}

	// Try to read in the cert
	certContents, err := ioutil.ReadFile(cert)
	if err != nil {
		return nil, err
	}

	// Try to read in the key
	keyContents, err := ioutil.ReadFile(key)
	if err != nil {
		return nil, err
	}

	var caContents []byte
	if caCerts != "" {
		caContents, err = ioutil.ReadFile(caCerts)

		if err != nil {
			return nil, err
		}
	}

	return loadClientCert(keyContents, certContents, caContents)
}

func loadClientCert(key, cert, caCerts []byte) (*tls.Config, error) {

	roots := x509.NewCertPool()

	// Only append caCerts if not nil
	if caCerts != nil {
		if ok := roots.AppendCertsFromPEM(caCerts); !ok {
			return nil, errors.New("Cannot add cert data to new cert pool")
		}
	}

	// Load in the certificate/key pair
	clientCert, err := tls.X509KeyPair(cert, key)
	if err != nil {
		return nil, err
	}

	return &tls.Config{
		RootCAs:      roots,
		Certificates: []tls.Certificate{clientCert},
	}, nil
}

type Error struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Detail  error  `json:"detail"`
	Code    int    `json:"code"`
}

func NewError(message string, detail error, code int) Error {
	return Error{
		Status:  "error",
		Message: message,
		Detail:  detail,
		Code:    code,
	}
}
