package mqttutil

type Broker struct {
	BrokerURI         string
	Username          string
	Password          string
	CertFile          string
	KeyFile           string
	CaChainFile       string
	ClientID          string
	PersistentSession bool
}
