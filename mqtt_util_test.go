package mqttutil

import (
	"testing"
)

func Test_JoinTopic(t *testing.T) {
	assertEqual(t, "", JoinTopic("/"))
	assertEqual(t, "/test", JoinTopic("/test"))
	assertEqual(t, "test", JoinTopic("test/"))
	assertEqual(t, "test/topic", JoinTopic("test/", "/topic"))
	assertEqual(t, "test/topic/t2", JoinTopic("test/", "/topic", "/t2/"))
}

func assertEqual(t *testing.T, a interface{}, b interface{}) {
	if a != b {
		t.Fatalf("%s != %s", a, b)
	}
}
